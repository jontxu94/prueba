package com.example.ik_2dm3.calculadora;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Calculadora extends AppCompatActivity {

    private TextView idEToperando1;
    private TextView idEToperando2;
    private TextView idETresultado;
    private String aux;
    private String aux2;
    private int numaux;
    private int numaux2;
    private int numaux3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        idEToperando1 = (EditText) findViewById(R.id.idEToperando1);
        idEToperando2 = (EditText) findViewById(R.id.idEToperando2);
        idETresultado = (EditText) findViewById(R.id.idETresultado);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calculadora, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void sumar(View View){
        if(idEToperando1.getText().toString().isEmpty() || idEToperando2.getText().toString().isEmpty()){
            aux=idEToperando1.getText().toString();
            aux2=idEToperando2.getText().toString();
            numaux = Integer.parseInt(aux);
            numaux2=Integer.parseInt(aux2);
            numaux3=numaux+numaux2;
            idETresultado.setText(""+numaux3);

        }else{
            Toast.makeText(getApplicationContext(), "No ha introducido todos los datos, asegurese que que tanto el operando uno como el operando 2 no estan vacio y vuelva a intentarlo",
                    Toast.LENGTH_LONG).show();
        }

    }
   public void restar(View View){
       if(idEToperando1.getText().toString().isEmpty() || idEToperando2.getText().toString().isEmpty()) {
           aux = idEToperando1.getText().toString();
           aux2 = idEToperando2.getText().toString();
           numaux = Integer.parseInt(aux);
           numaux2 = Integer.parseInt(aux2);
           numaux3 = numaux - numaux2;
           idETresultado.setText("" + numaux3);
       }else{
           Toast.makeText(getApplicationContext(), "No ha introducido todos los datos, asegurese que que tanto el operando uno como el operando 2 no estan vacio y vuelva a intentarlo",
                   Toast.LENGTH_LONG).show();
       }

    }
    public void multiplicar(View View){
        if(idEToperando1.getText().toString().isEmpty() || idEToperando2.getText().toString().isEmpty()) {
            aux = idEToperando1.getText().toString();
            aux2 = idEToperando2.getText().toString();
            numaux = Integer.parseInt(aux);
            numaux2 = Integer.parseInt(aux2);
            numaux3 = numaux * numaux2;
            idETresultado.setText("" + numaux3);
        }else{
            Toast.makeText(getApplicationContext(), "No ha introducido todos los datos, asegurese que que tanto el operando uno como el operando 2 no estan vacio y vuelva a intentarlo",
                    Toast.LENGTH_LONG).show();
        }
    }
    public void dividir(View View) {
        if (idEToperando1.getText().toString().isEmpty() || idEToperando2.getText().toString().isEmpty()) {
            aux = idEToperando1.getText().toString();
            aux2 = idEToperando2.getText().toString();
            numaux = Integer.parseInt(aux);
            numaux2 = Integer.parseInt(aux2);
            if(numaux2!=0) {
                numaux3 = numaux / numaux2;
                idETresultado.setText("" + numaux3);
            }else{

                Toast.makeText(getApplicationContext(), "Esta intentando dividir por 0, cambie el valor del operando 2 y vuelva a intentarlo",
                        Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), "No ha introducido todos los datos, asegurese que que tanto el operando uno como el operando 2 no estan vacio y vuelva a intentarlo",
                    Toast.LENGTH_LONG).show();
        }
    }
}
